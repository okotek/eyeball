module eyeball

go 1.22.0

replace gitlab.com/okotek/okoerr => ../okoerr

replace gitlab.com/okotek/okoframe => ../okoframe

require (
	gitlab.com/okotek/okoerr v0.0.0-00010101000000-000000000000
	gitlab.com/okotek/okoframe v0.0.0-00010101000000-000000000000
	gocv.io/x/gocv v0.38.0
)
