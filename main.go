package main

import (
	"encoding/gob"
	"flag"
	"fmt"
	"net"
	"sync"

	"gitlab.com/okotek/okoerr"
	"gitlab.com/okotek/okoframe"
	"gocv.io/x/gocv"
)

func main() {

	var stopper sync.WaitGroup
	stopper.Add(69420)

	addr := flag.String("sendoffTarget", "", "ipv4 address of recieving node. No port needed.")
	flag.Parse()

	frameChan := make(chan okoframe.Frame)
	go runCamera(frameChan, 0)
	go runMatSendoff(frameChan, *addr)

	stopper.Wait()
}

func runCamera(output chan gocv.Mat, camIndex int) {

	readMat := gocv.NewMat()

	cam, getCamErr := gocv.VideoCaptureDevice(camIndex)
	if getCamErr != nil {
		okoerr.ReportError("Couldn't get camera for runCamera function", "resource", getCamErr)
		return
	}

	for {
		readErr := cam.Read(&readMat)
		if readErr != nil {
			fmt.Printf("\rGot read error with webcam read: %v", readErr)
		}
	}

}

func runMatSendoff(input chan gocv.Mat, targAddr string) {

	for tmpMat := range input {
		go func(tMat gocv.Mat, targ string) {

			con, conErr := net.Dial("tcp", targAddr+":8645")
			if conErr != nil {
				okoerr.ReportError("couldn't dial for mat transfer", "net", conErr)
			}

			if encErr := gob.NewEncoder(con).Encode(&tMat); encErr != nil {
				okoerr.ReportError("failed encode for mat transfer", "net", encErr)
			}

		}(tmpMat, targAddr)
	}

}
